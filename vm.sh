#!/bin/bash

# VARIABLES
source $(dirname "$0")/config.conf

# SCRIPT
orange='\E[33;33m'                                                                                                   
red='\E[31;31m'                                                                                                      
black='\E[30;47m'                                                                                                    
green='\E[32;32m'                                                                                                    
NC='\033[0m'

function loading {
    i=0
    while ! [ $i -ge 8 ] ; do
	echo -e $green '/ loading.'; sleep 0.1; clear;                                                                                            
	echo -e $green '- loading..'; sleep 0.1; clear;                                                                                            
	echo -e $green '\ loading...'; sleep 0.1; clear;                                                                                          
	echo -e $green '| loading.'; sleep 0.1; clear;                                                                                            
	echo -e $green '/ loading..'; sleep 0.1; clear;                                                                                            
	echo -e $green '- loading..'; sleep 0.1; clear;
	echo -e $green '\ loading...'; sleep 0.1; clear;                                                                                         
	echo -e $green '| loading.'; sleep 0.1; clear; 
	((i=i+1));
	
    done

    echo -e $green 'VM LOADED!'; sleep 3; clear;      
}

function exit_prog {
    exit;
}

function main_menu {
    
    echo -e $green "What do you want to do?" $NC                                                                          
    echo -e $black "MAIN MENU" $NC                                                                                        
    select CRFE in "Create_an_HDD" "Run_a_VM" "First_Setup" "Exit"; do  
	case $CRFE in                                                                                                 
            Create_an_HDD )
		read -p "How would you like to name the HDD? " answer &&
		    read -p "How big do you want it? " GB &&    
		    qemu-img create -f qcow2 $PATH_HDD/$answer.qcow $GB\G ; break;;
	    
	    Run_a_VM ) echo " " && ls $PATH_HDD | grep .qcow && echo " " && read -p "Which one? " vm && (kvm -hda $PATH_HDD/$vm.qcow -m 2048 -smp 4 &
													 loading); break;;
	    First_Setup ) echo " " && ls $PATH_HDD | grep .qcow && echo " " && read -p "Choose the HDD " HDD &&
				echo " " && ls $PATH_ISO | grep .iso && read -p "Choose the iso " iso &&
				kvm -hda $PATH_HDD/$HDD.qcow -cdrom $PATH_ISO/$iso.iso -boot d -m 2048 -smp 4 &> /dev/null ; break;;    
	    Exit ) kill $PPID; break;;                                                                                          
	esac    
    done

}
clear
{ sleep 0.1; } | echo -e "$green ||  =======================================  || $NC"                                                                  
{ sleep 0.1; } | echo -e "$green ||$NC  $red \\            // $NC $orange||\\            //|| S $NC $green ||$NC"                                                          
{ sleep 0.1; } | echo -e "$green ||$NC  $red  \\          // $NC  $orange|| \\          // || E $NC $green ||$NC"                                                         
{ sleep 0.1; } | echo -e "$green ||$NC  $red   \\        // $NC   $orange||  \\        //  || L $NC $green ||$NC"                                                          
{ sleep 0.1; } | echo -e "$green ||$NC  $red    \\      // $NC    $orange||   \\      //   || E $NC $green ||$NC"
{ sleep 0.1; } | echo -e "$green ||$NC  $red     \\    // $NC     $orange||    \\    //    || C $NC $green ||$NC"
{ sleep 0.1; } | echo -e "$green ||$NC  $red      \\  // $NC      $orange||     \\  //     || T $NC $green ||$NC"
{ sleep 0.1; } | echo -e "$green ||$NC  $red       \\// $NC       $orange||      \\//      || ! $NC $green ||$NC"                                         
{ sleep 0.1; } | echo -e "$green ||  =======================================  || $NC"        

echo " "
main_menu

while main_menu
                                                                                                            
do                                                                                                                    
    main_menu
    
done


######  NOTES #########
#CREATE HDD
#qemu-img create -f qcow2 winzozz.qcow 40G

#qemu-system-x86_64 -hda debian.img -cdrom debian-testing-amd64-netinst.iso -boot d -m 512

#ANDROID
#kvm -hda andr.qcow -m 2048 -smp 4

#WINZOZZ
#kvm -hda winzozz.qcow -m 3000 -smp 4
